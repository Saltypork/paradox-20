
=begin

もんむす・くえすと！ＲＰＧ
　トリス修正K  ver6  2015/02/11



機能一覧　説明は下　このverで新規追加したものは●　変更したものは○
○キャラ図鑑のCG閲覧の開始時に、BGMがマップのものに戻ったのを修正
・キャラ/魔物図鑑のCG閲覧で、CG切り替え時のフェードを一瞬に
・キャラ/魔物図鑑のCG閲覧で、最後まで閲覧すると1枚目に戻るのではなく閲覧終了
・アビリティ画面でアビリティを外した直後、コストを超えて装備できたのを修正
・アビリティ画面にボタンヘルプ用ウインドウを追加
・スキル画面にボタンヘルプ用ウインドウを追加


機能　説明

・キャラ/魔物図鑑のCG閲覧で、最後まで閲覧すると1枚目に戻るのではなく閲覧終了
　最後のCGの表示中に「→ボタンか決定ボタン」を入力 : 閲覧終了し図鑑に戻る
　最初のCGの表示中に「←ボタン」を入力 : 何もしない

=end

#==============================================================================
# ■ Scene_Library
#==============================================================================
class Scene_Library < Scene_MenuBase
  #--------------------------------------------------------------------------
  # ● 終了処理 図鑑/本体
  #--------------------------------------------------------------------------
  def terminate
    super
    replay_bgm_and_bgs unless SceneManager.scene_is?(Scene_CGViewer) ||
                              SceneManager.scene_is?(Scene_ActorCGViewer) ||
                              SceneManager.scene_is?(Scene_Novel)
  end
end
#==============================================================================
# ■ Sprite_CGViewer
#==============================================================================
class Sprite_CGViewer < Sprite_Base
  #--------------------------------------------------------------------------
  # ● フレーム更新 基盤システム/画像ビューア
  #--------------------------------------------------------------------------
  def update
    super
    self.opacity += @current ? 255 : -255
  end
end
#==============================================================================
# ■ Scene_CGViewer
#==============================================================================
class Scene_CGViewer < Scene_Base
  #--------------------------------------------------------------------------
  # ● ＣＧの変更 基盤システム/画像ビューア
  #--------------------------------------------------------------------------
  def process_cg_change
    return unless current_cg_sprites.first.full_view?
    if Input.trigger?(:RIGHT) || Input.trigger?(:C)
      max = cg_view_image[:draw].size
      if @select + 1 == max
        return_scene
      else
        @select = @select + 1
      end
    elsif Input.trigger?(:LEFT)
      if @select == 0
        
      else
        @select = @select - 1
      end
    end
    refresh_cg_sprites
  end
end

#==============================================================================
# ■ Foo::Ability::Window_EquipAbility
#==============================================================================
class Foo::Ability::Window_EquipAbility < Window_Command
  #--------------------------------------------------------------------------
  # ● リフレッシュ
  #--------------------------------------------------------------------------
  def refresh
    super
    select(self.index)
  end
end

#==============================================================================
# ■ Help
#==============================================================================
class << Help
  #--------------------------------------------------------------------------
  # ● アビリティ画面の上部ヘルプメッセージ ベース/Module 239 toris
  #--------------------------------------------------------------------------
  def ability
    "Please choose an ability type."
  end
  #--------------------------------------------------------------------------
  # ● アビリティ画面の下部ヘルプメッセージ ベース/Module 239
  #--------------------------------------------------------------------------
  def ability_key
    "#{Vocab.key_c}:Select　#{Vocab.key_b}:Cancel　#{Vocab.key_a}:Remove"
  end
  #--------------------------------------------------------------------------
  # ● スキル画面の下部ヘルプメッセージ ベース/Module 239
  #--------------------------------------------------------------------------
  def skill_type_key
    "#{Vocab.key_c}:Select　#{Vocab.key_b}:Cancel　#{Vocab.key_a}:Hide in battle"
  end
end
#==============================================================================
# ■ Foo::Ability::Window_StandAbility
#==============================================================================
class Foo::Ability::Window_StandAbility < Window_Command
  #--------------------------------------------------------------------------
  # ● ウィンドウ高さの取得 画面/アビリティ編集 188
  #--------------------------------------------------------------------------
  def window_height
    Graphics.height - 216 - fitting_height(1)
    
    # Added To Correct @help_window Being 4 Lines
    #Graphics.height - 216 - fitting_height(3)
  end
end
#==============================================================================
# ■ Foo::Ability::Window_EquipAbility
#==============================================================================
class Foo::Ability::Window_EquipAbility < Window_Command
  #--------------------------------------------------------------------------
  # ● ウィンドウ高さの取得 画面/アビリティ編集 282
  #--------------------------------------------------------------------------
  def window_height
    Graphics.height - 216 - fitting_height(1)
    
    # Added To Correct @help_window Being 4 Lines
    #Graphics.height - 216 - fitting_height(3)
  end
end
#==============================================================================
# ■ Foo::Ability::Window_AbilityType
#==============================================================================
class Foo::Ability::Window_AbilityType < Window_Selectable
  #--------------------------------------------------------------------------
  # ● ヘルプウィンドウの更新 画面/アビリティ編集 505 消去
  #--------------------------------------------------------------------------
  def update_help
    super
    @help_window.set_text(Help.ability)
  end   
end
#==============================================================================
# ■ Scene_Ability
#==============================================================================
class Scene_Ability < Scene_MenuBase
  #--------------------------------------------------------------------------
  # ● 開始処理 画面/アビリティ編集 518
  #--------------------------------------------------------------------------
  def start  
    super
    #create_help_window
    create_ability_help_window
    
    create_stand_ability_window
    create_equip_ability_window
    create_ability_type_window
    create_key_help_window
  end
  #--------------------------------------------------------------------------
  # ● 操作ヘルプウィンドウの生成 画面/アビリティ編集 toris
  #--------------------------------------------------------------------------
  def create_key_help_window
    @key_help_window = Window_Help.new(1)
    @key_help_window.y = Graphics.height - @key_help_window.fitting_height(1)
    @key_help_window.set_text(Help.ability_key)
  end
end
#==============================================================================
# ■ Scene_Skill
#==============================================================================
class Scene_Skill < Scene_ItemBase
  #--------------------------------------------------------------------------
  # ● 開始処理 ベース/Scene 139 toris
  #--------------------------------------------------------------------------
  def start
    super
    create_help_window
    create_command_window
    create_status_window
    create_item_window
    create_key_help_window
  end
  #--------------------------------------------------------------------------
  # ● 操作ヘルプウィンドウの生成 ベース/Scene 139 toris
  #--------------------------------------------------------------------------
  def create_key_help_window
    @key_help_window = Window_Help.new(1)
    @key_help_window.y = Graphics.height - @key_help_window.fitting_height(1)
    @key_help_window.viewport = @viewport
    @key_help_window.set_text(Help.skill_type_key)
  end
  #--------------------------------------------------------------------------
  # ● コマンド［スキル］
  #--------------------------------------------------------------------------
  def command_skill
    @item_window.activate
    @item_window.select_last
    @key_help_window.hide
  end
  #--------------------------------------------------------------------------
  # ● アイテム［キャンセル］
  #--------------------------------------------------------------------------
  def on_item_cancel
    @item_window.unselect
    @command_window.activate
    @key_help_window.show
  end
end